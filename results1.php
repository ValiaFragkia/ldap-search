<?php
	/* list of results */
	 
	require("page.php");
	
	//start session
	session_start();
	
	$resultsPage = new Page();	
	
	$resultsPage -> DisplayUpper();
	
	//alert message if all fields are empty
	if ((empty($_POST["uid"])) && (empty($_POST["name"])) && 
		(empty($_POST["surname"])) && (empty($_POST["amka"]))) {
			echo "<script src='js/alert_if_empty.js'></script>";
	}
	
	//print results
	function printResult($ds, $search)
	{
		echo "<p id='res'>Αποτελέσματα</p>"; //display before printing only
		echo "<div id='results'>";
		echo "<ul>";
		$k = 0;
		for ($entryID=ldap_first_entry($ds,$search); $entryID!=false; 
			 $entryID=ldap_next_entry($ds,$entryID)){
				$attrs = ldap_get_attributes($ds, $entryID);
				//pass object to the next page 'attrs.php'
				$_SESSION['object'][$k] = $attrs;
				
				echo "<li><a href='attrs.php?count=$k' target='_blank'>";
				$k ++;
			
				if (isset($attrs['cn;lang-el'][0])) 
				{
					echo $attrs['cn;lang-el'][0];
				} else if (isset($attrs['cn;lang-en'][0]))
				{
					echo $attrs['cn;lang-en'][0];
				} else if (isset($attrs['cn'][0]))
				{	
					echo $attrs['cn'][0];	
				} else if (isset($attrs['displayName;lang-el'][0]))
				{
					echo $attrs['displayName;lang-el'][0];
				} else if (isset($attrs['displayName;lang-en'][0]))
				{	
					echo $attrs['displayName;lang-en'][0];
				} else if (isset($attrs['displayName'][0]))
				{	
					echo $attrs['displayName'][0];	
				} else if (isset($attrs['givenName;lang-el'][0]))
				{	
					echo $attrs['givenName;lang-el'][0];
				} else if (isset($attrs['givenName;lang-en'][0]))
				{
					echo $attrs['givenName;lang-en'][0];
				} else if (isset($attrs['givenName'][0]))
				{
					echo $attrs['givenName'][0];		
				}
				echo "</a>";
				echo "</li>";
		}
				echo "</ul>";	
				echo "</div>";	
	}
	
	/* Compares the contents of two result arrays and keeps the common ones
	based on the unique email attribute. Prints on the screen the links to the attributes. */
	function compareResults($ds, $search1, $search2) 
	{
		echo "<ul>";
		$k = 0;
		$found = false;
		for ($entryID1=ldap_first_entry($ds,$search1); $entryID1!=false; 
			 $entryID1=ldap_next_entry($ds,$entryID1)){
				for ($entryID2=ldap_first_entry($ds,$search2); $entryID2!=false; 
					$entryID2=ldap_next_entry($ds,$entryID2)){
					$attrs1 = ldap_get_attributes($ds, $entryID1);
					$attrs2 = ldap_get_attributes($ds, $entryID2);
					
				//	echo "mail1: ".$attrs1["mail"][0]." mail2: ". $attrs2["mail"][0];
					if ((isset($attrs1["mail"][0])) && isset($attrs2["mail"][0])){
						$num = strcmp($attrs1["mail"][0], $attrs2["mail"][0]);		
						if ($num == 0)
						{
						//	echo "mail: ".$attrs1["mail"][0]." ". $attrs2["mail"][0];					
							$found = true;
						}					
				
						if ($found)
						{
							echo "<li><a href='attrs.php?count=$k' target='_blank'>";
							echo $attrs1['cn;lang-el'][0]."<br>";
							echo "</a>";
							echo "</li>";
							//pass object to the next page 'attrs.php'
							$_SESSION['object'][$k] = $attrs1;
							$k ++;
							$found = false;
						}	
					}
				}
		}
		echo "</ul>";			
	}
	
	/* compare 2 result arrays, returns an array with the mail attribute of the records 
	in common*/
	function compareResults2($ds, $search1, $search2) 
	{
		$found = false;
		$i = 0;
		for ($entryID1=ldap_first_entry($ds,$search1); $entryID1!=false; 
			 $entryID1=ldap_next_entry($ds,$entryID1)){
				for ($entryID2=ldap_first_entry($ds,$search2); $entryID2!=false; 
					$entryID2=ldap_next_entry($ds,$entryID2)){
					$attrs1 = ldap_get_attributes($ds, $entryID1);
					$attrs2 = ldap_get_attributes($ds, $entryID2);
					
				//	echo "mail1: ".$attrs1["mail"][0]." mail2: ". $attrs2["mail"][0];
					if ((isset($attrs1["mail"][0])) && isset($attrs2["mail"][0])){
						$num = strcmp($attrs1["mail"][0], $attrs2["mail"][0]);		
						if ($num == 0)
						{
						//	echo "mail: ".$attrs1["mail"][0]." ". $attrs2["mail"][0];
							$emailArray[$i] = $attrs1["mail"][0];
							$i++;
							$found = true;
						}					
				
						if ($found)
						{
						//	echo $attrs1['cn;lang-el'][0]."<br>";
							
							$found = false;
						}	
					}
				}
		}
		if (isset($emailArray)){
			return $emailArray;
		}
	}
	
	/* Compares the contents of an email array and a result array and keeps the common ones. 
	Prints on the screen the links to the attributes. */
	function compareResultWithEmailArray($ds, $search1, $emailArray) 
	{
		echo "<ul>";
		$k = 0;
		$found = false;
		for ($entryID1=ldap_first_entry($ds,$search1); $entryID1!=false; 
			 $entryID1=ldap_next_entry($ds,$entryID1)){
				for ($i = 0; $i<count($emailArray);$i++){
					$attrs1 = ldap_get_attributes($ds, $entryID1);
					
				//	echo "mail1: ".$attrs1["mail"][0]." mail2: ". $emailArray[$i]."<br>";
				//	echo "i:". $i;
					if (isset($attrs1["mail"][0])){
						$num = strcmp($emailArray[$i], $attrs1["mail"][0]);		
						if ($num == 0)
						{
					//		echo "found mail: ".$attrs1["mail"][0]." ". $emailArray[$i];								
							$found = true;
						}					
				
						if ($found)
						{
							echo "<li><a href='attrs.php?count=$k' target='_blank'>";
							echo $attrs1['cn;lang-el'][0]."<br>";
							echo "</a>";
							echo "</li>";
							//pass object to the next page 'attrs.php'
							$_SESSION['object'][$k] = $attrs1;
							$k ++;
							$found = false;
						}	
					}
				}
		}
		echo "</ul>";			
	}
		
	/* Compares the contents of an email array and a result array and keeps the common ones. 
	Prints on the screen the links to the attributes. */
	function compareResultWithEmailArray2($ds, $search1, $emailArray) 
	{
		$k = 0;	//counter in the new array
		$found = false;
		for ($entryID1=ldap_first_entry($ds,$search1); $entryID1!=false; 
			 $entryID1=ldap_next_entry($ds,$entryID1)){
				for ($i = 0; $i<count($emailArray);$i++){
					$attrs1 = ldap_get_attributes($ds, $entryID1);
					
					//echo "mail1: ".$attrs1["mail"][0]." mail2: ". $emailArray[$i]."<br>";
					//echo "i:". $i;
					if (isset($attrs1["mail"][0])){
						$num = strcmp($emailArray[$i], $attrs1["mail"][0]);		
						if ($num == 0)
						{
						//	echo "found mail: ".$attrs1["mail"][0]." ". $emailArray[$i];								
							$found = true;
						}					
				
						if ($found)
						{
							$emailArrayNew[$k] = $emailArray[$i];  // put the common value in the new array
							$k++;
							$found = false;
						}	
					}
				}
		}
		if (isset($emailArrayNew)){
			return $emailArrayNew;
		}		
	}
	
	/* compares an array returned from the previous method with a result set 
	   prints the result */
	function compare($ds, $search1, $search2) 
	{
		$found = false;
		$i = 0;
		for ($entryID1=ldap_first_entry($ds,$search1); $entryID1!=false; 
			 $entryID1=ldap_next_entry($ds,$entryID1)){
				for ($entryID2=ldap_first_entry($ds,$search2); $entryID2!=false; 
					$entryID2=ldap_next_entry($ds,$entryID2)){
					$attrs1 = ldap_get_attributes($ds, $entryID1);
					$attrs2 = ldap_get_attributes($ds, $entryID2);
					
					//echo "mail1: ".$attrs1["mail"][0]." mail2: ". $attrs2["mail"][0];
					if ((isset($attrs1["mail"][0])) && isset($attrs2["mail"][0])){
						$num = strcmp($attrs1["mail"][0], $attrs2["mail"][0]);		
						if ($num == 0)
						{
						//	echo "mail: ".$attrs1["mail"][0]." ". $attrs2["mail"][0];
							$result[$i] = $attrs1["mail"][0];
							$i++;
							$found = true;
						}					
				
						if ($found)
						{
							echo $attrs1['cn;lang-el'][0]."<br>";
							
							$found = false;
						}	
					}
				}
		}
		return $result;
	}
	
	if(!(empty($_POST['uid'])))
	{
		/* connect to ldap  */
	
		$ldap_host = 'ds.uoc.gr';
		$ldap_port = 409;
	
		$ds = ldap_connect($ldap_host, $ldap_port)
				or die("Could not connect to LDAP server.");

		$bd = ldap_bind($ds) 
				or die ("System couldn't bind the connection!");
		
		$dn = "dc=uoc,dc=gr";
		
		//search for uid
		$filter = "(&(uid=".$_POST['uid']."*))";
	
		$search1 = ldap_search($ds, $dn, $filter);  //result
		$result1 = ldap_get_entries($ds, $search1);	//get result
	//	echo $result1["count"];
		
//		printResult($ds, $search1);
	} 
	
	if(!(empty($_POST['name']))) 
	{
		//if english characters
		if (preg_match('/[A-Za-z]/', $_POST['name']))
		{
	
			/* connect to ldap  */
	
			$ldap_host = 'ds.uoc.gr';
			$ldap_port = 409;
		
			$ds = ldap_connect($ldap_host, $ldap_port)
					or die("Could not connect to LDAP server.");

			$bd = ldap_bind($ds) 
					or die ("System couldn't bind the connection!");
		
			$dn = "dc=uoc,dc=gr";
		
			//search 
			$filter = "(&(givenName=".$_POST['name']."*))";
	
			$search2 = ldap_search($ds, $dn, $filter);  //result
			$result2 = ldap_get_entries($ds, $search2);	//get result
			
	//		printResult ($ds, $search2);
		}
	
		if (preg_match('/[Α-Ωα-ω]/', $_POST['name']))
		{
			/* connect to ldap  */
	
			$ldap_host = 'ds.uoc.gr';
			$ldap_port = 409;
	
			$ds = ldap_connect($ldap_host, $ldap_port)
					or die("Could not connect to LDAP server.");
	
			$bd = ldap_bind($ds) 
					or die ("System couldn't bind the connection!");
			
			$dn = "dc=uoc,dc=gr";
		
			//search 
			$filter = "(&(givenName=".$_POST['name']."*))";
		
			$search2 = ldap_search($ds, $dn, $filter);  //result
			$result2 = ldap_get_entries($ds, $search2);	//get result
			
	//		printResult ($ds, $search2);
		}
	}
	
	if(!(empty($_POST['surname']))) 
	{
		//if english characters
		if (preg_match('/[A-Za-z]/', $_POST['surname']))
		{
	
			/* connect to ldap  */
	
			$ldap_host = 'ds.uoc.gr';
			$ldap_port = 409;
	
			$ds = ldap_connect($ldap_host, $ldap_port)
					or die("Could not connect to LDAP server.");

			$bd = ldap_bind($ds) 
					or die ("System couldn't bind the connection!");
		
			$dn = "dc=uoc,dc=gr";
		
			//search
			$filter = "(&(sn=".$_POST['surname']."*))";
			
			$search3 = ldap_search($ds, $dn, $filter);  //result
			$result3 = ldap_get_entries($ds, $search3);	//get result
		
		//	printResult ($ds, $search3);
			
		}
	
		//if greek characters
		if (preg_match('/[Α-Ωα-ω]/', $_POST['surname']))
		{
			/* connect to ldap  */
	
			$ldap_host = 'ds.uoc.gr';
			$ldap_port = 409;
	
			$ds = ldap_connect($ldap_host, $ldap_port)
					or die("Could not connect to LDAP server.");

			$bd = ldap_bind($ds) 
					or die ("System couldn't bind the connection!");
		
			$dn = "dc=uoc,dc=gr";
		
			//search 
			$filter = "(&(sn=".$_POST['surname']."*))";
	
			$search3 = ldap_search($ds, $dn, $filter);  //result
			$result3 = ldap_get_entries($ds, $search3);	//get result
			
	//		printResult ($ds, $search3);
		
		}
	}
	
	//cross-validate results depending on the input
	if (isset($result1) && isset($result2) && isset($result3) && isset($result4))
	{
		$emailArray = compareResults2($ds, $search1, $search2);
		if (isset($emailArray)){
			$emailArrayNew = compareResultWithEmailArray2($ds, $search3, $emailArray);
		}
		if (isset($emailArrayNew)){
			compareResultWithEmailArray($ds, $search4, $emailArray);
		}
		
	} else if (isset($result1) && isset($result2)&& isset($result3))
	{
		$emailArray = compareResults2($ds, $search1, $search2);
		if (isset($emailArray)) {
			compareResultWithEmailArray($ds, $search3, $emailArray);
		}
		
	} else if (isset($result1) && isset($result2)&& isset($result4))
	{
		$emailArray = compareResults2($ds, $search1, $search2);
		if (isset($emailArray)) {
			compareResultWithEmailArray($ds, $search4, $emailArray);
		}
	} else if (isset($result1) && isset($result3)&& isset($result4))
	{
		$emailArray = compareResults2($ds, $search1, $search3);
		if (isset($emailArray)) {
			compareResultWithEmailArray($ds, $search4, $emailArray);
		}
	} else if (isset($result2) && isset($result3)&& isset($result4))
	{
		$emailArray = compareResults2($ds, $search2, $search3);
		if (isset($emailArray)) {
			compareResultWithEmailArray($ds, $search4, $emailArray);
		}
	} else if (isset($result1) && isset($result2))
	{
		compareResults($ds, $search1, $search2);				
	} else if (isset($result1) && isset($result3))
	{
		compareResults($ds, $search1, $search3);	
	} else if (isset($result1) && isset($result4))
	{
		compareResults($ds, $search1, $search4);	
	} else if (isset($result2) && isset($result3))
	{
		compareResults($ds, $search2, $search3);	
	} else if (isset($result2) && isset($result4))
	{
		compareResults($ds, $search2, $search4);	
	} else if (isset($result3) && isset($result4))
	{
		compareResults($ds, $search3, $search4);	
	} else if (isset($result1))
	{
		if ($result1["count"] > 200) {
			echo "<script src='js/too_many_results.js'></script>";
			exit;
		}
		printResult($ds, $search1);	
	} else if (isset($result2))
	{
		if ($result2["count"] > 200) {
			echo "<script src='js/too_many_results.js'></script>";
			exit;
		}
		printResult($ds, $search2);
	} else if (isset($result3))
	{
		if ($result3["count"] > 200) {
			echo "<script src='js/too_many_results.js'></script>";
			exit;
		}
		printResult($ds, $search3);
	} else if (isset($result4))
	{
		if ($result4["count"] > 200) {
			echo "<script src='js/too_many_results.js'></script>";
			exit;
		}
		printResult($ds, $search4);
	}
	
	$resultsPage -> DisplayBottom();
?>