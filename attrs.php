<?php 
	//Using GET	
	require("page.php");
	
	//start session
	session_start();
	
	$attrsPage = new Page();	
		
	$attrsPage -> DisplayUpper();
		
	$count = $_GET['count'];
	$attrs = $_SESSION['object'][$count];
	
	$k = -1; //number of rows for even or odd table row
	echo "<div id='table'>";
	echo "<table>";
	for ($i=0; $i < $attrs["count"]; $i++) { //number of attributes
		$k++;
		if (($k%2) == 0) {
			echo "<tr class='even'><th>".$attrs[$i] . ": </th>";
		} else {
			echo "<tr class='odd'><th>".$attrs[$i] . ": </th>";
		}
		if(strcmp($attrs[$i],"createTimestamp") == 0 || (strcmp($attrs[$i],"modifyTimestamp") == 0)) {
			echo "<td>".substr($attrs[$attrs[$i]][0], 0, 4)."-".substr($attrs[$attrs[$i]][0], 4, 2)
				."-".substr($attrs[$attrs[$i]][0], 6, 2)." ".substr($attrs[$attrs[$i]][0], 8, 2).
				":".substr($attrs[$attrs[$i]][0], 10, 2).":".substr($attrs[$attrs[$i]][0], 12, 2)."</td>";
		} else{ // if ((strcmp($attrs[$i],"objectClass") != 0) && (strcmp($attrs[$i],"uocServices") != 0)) {
			echo "<td>".$attrs[$attrs[$i]][0]."  </td></tr>";
			for ($j=1; $j < $attrs[$attrs[$i]]["count"]; $j+=2) { //number of values for an attribute
				$k++;
				if (($k%2) == 0){
					echo "<tr class='even'><th></th><td>".$attrs[$attrs[$i]][$j]."  </td></tr>";
				} else {
					echo "<tr class='odd'><th></th><td>".$attrs[$attrs[$i]][$j]."  </td></tr>";
				}if (($j+1) < $attrs[$attrs[$i]]["count"]) { //number of values for an attribute
					$k++;
					if (($k%2) == 0){
						echo "<tr class='even'><th></th><td>".$attrs[$attrs[$i]][$j+1]."  </td></tr>";
					} else {
						echo "<tr class='odd'><th></th><td>".$attrs[$attrs[$i]][$j+1]."  </td></tr>";
					}
				}
			}
//		} else {
			//	echo "<td>".$attrs[$attrs[$i]][0]."  </td>";
		} 
		echo "</tr>";
	}
	echo "</table>";
	echo "</div>";
	
	$attrsPage -> DisplayBottom();
	
?>