<?php //alert message if all fields are empty
	/* list of results */
	 
	require("page.php");
	
	//start session
	session_start();
	
	$resultsPage = new Page();	
	
	$resultsPage -> DisplayUpper();
	
	if (empty($_POST["edet"])) {
		echo "<script src='js/alert_if_empty2.js'></script>";
	}
	
	//print results
	function printResult($ds, $search)
	{
		echo "<p id='res'>Αποτελέσματα</p>"; //display before printing only
		echo "<div id='results'>";
		echo "<ul>";
		$k = 0;
		for ($entryID=ldap_first_entry($ds,$search); $entryID!=false; 
			 $entryID=ldap_next_entry($ds,$entryID)){
				$attrs = ldap_get_attributes($ds, $entryID);
				//pass object to the next page 'attrs.php'
				$_SESSION['object'][$k] = $attrs;
				
				echo "<li><a href='attrs.php?count=$k' target='_blank'>";
				$k ++;
			
				if (isset($attrs['cn;lang-el'][0])) 
				{
					echo $attrs['cn;lang-el'][0];
				} else if (isset($attrs['cn;lang-en'][0]))
				{
					echo $attrs['cn;lang-en'][0];
				} else if (isset($attrs['cn'][0]))
				{	
					echo $attrs['cn'][0];	
				} else if (isset($attrs['displayName;lang-el'][0]))
				{
					echo $attrs['displayName;lang-el'][0];
				} else if (isset($attrs['displayName;lang-en'][0]))
				{	
					echo $attrs['displayName;lang-en'][0];
				} else if (isset($attrs['displayName'][0]))
				{	
					echo $attrs['displayName'][0];	
				} else if (isset($attrs['givenName;lang-el'][0]))
				{	
					echo $attrs['givenName;lang-el'][0];
				} else if (isset($attrs['givenName;lang-en'][0]))
				{
					echo $attrs['givenName;lang-en'][0];
				} else if (isset($attrs['givenName'][0]))
				{
					echo $attrs['givenName'][0];		
				}
				echo "</a>";
				echo "</li>";
		}
				echo "</ul>";	
				echo "</div>";	
	}
	
	if(!(empty($_POST['edet'])))
	{
		/* connect to ldap  */
	
		$ldap_host = 'ds.uoc.gr';
		$ldap_port = 409;
	
		$ds = ldap_connect($ldap_host, $ldap_port)
				or die("Could not connect to LDAP server.");

		$bd = ldap_bind($ds) 
				or die ("System couldn't bind the connection!");
		
		$dn = "dc=uoc,dc=gr";
		
		//search for edu Person entitl. attribute
		$filter = "(&(eduPersonEntitlement=".$_POST['edet']."*))";
	
		$search1 = ldap_search($ds, $dn, $filter);  //result
		$result1 = ldap_get_entries($ds, $search1);	//get result
		if ($result1["count"] > 200) {
			echo "<script src='js/too_many_results.js'></script>";
			exit;
		}
		printResult($ds, $search1);
	} 
	
	$resultsPage -> DisplayBottom();
?>