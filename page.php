<?php
class Page
{
	public $errorMessage;
	public $resultsTitle;
	public $results;
	public $btn;
	public $title = "UOC LDAP Search Tool";
	
	 // class Page's operations
	public function __set($name, $value)
	{
		$this->$name = $value;
	}
  
	public function DisplayUpper()
    {
		echo "<html>\n<head>\n";
		$this -> DisplayTitle();
		$this -> DisplayStyles();
		echo "</head>\n<body>\n";
		$this -> DisplayHeader();
		$this -> DisplayForm();
		echo $this -> errorMessage;
		echo $this->resultsTitle;
		echo $this->results;
		echo $this->btn;
	}
	
	public function DisplayBottom()
    {
		$this -> DisplayFooter();
		echo "</body>\n</html>\n";
	}
		
	public function DisplayTitle()
	{
		echo "<title>".$this->title."</title>";
	}

	public function DisplayStyles()
	{	 
		?>   
		<link href="css/styles.css" type="text/css" rel="stylesheet">
		<?php
	}

	public function DisplayHeader()
	{ 
		?>   
		<section id="page">
			<!-- page header -->
			<header>
				<meta charset="utf-8">
				<img id="image-align-left" src="uoc_logo.png" alt="uoc logo" height="90" width="90" align = "left"/> 
				<h2>ΠΑΝΕΠΙΣΤΗΜΙΟ ΚΡΗΤΗΣ</h2>
				<img id="image-align-right" src="ucnet_logo.png" alt="uoc logo" height="100" width="240"/> 
			</header><!-- end header -->
		<?php
	}
	
	public function DisplayForm()
	{
		?>
		<section id="content">
			<form id="form1" action="results1.php" method="post">
				<p id="choose"><strong>Αναζήτηση</strong><br>
				<fieldset>
					<legend>Στοιχεία</legend>
					<label>Uid: </label><br /><input type="text" name="uid" size="30" maxlength="30"><br />
					<label>Όνομα: </label><br /> <span> (ελληνικοί ή λατινικοί χαρακτήρες) </span> <br /><input type="text" name="name" size="30" maxlength="40"><br />
					<label>Επώνυμο: </label> <br /> <span> (ελληνικοί ή λατινικοί χαρακτήρες) </span> <br /><input type="text" name="surname" size="30" maxlength="40"><br />
					<label>ΑΜΚΑ: </label><br /><input type="text" name="amka" size="30" id="amka" maxlength="30" placeholder="Το πεδίο είναι ανενεργό"><br />
				</fieldset><br />
				<input type="submit" name="submit1" value="Search">
			</form>
			<form class="form2" action="results2.php" method="post">
				<p id="choose"><strong>Ειδικές Αναζητήσεις</strong><br>
				<fieldset>
					<label>Email Alternate Address: </label><input type="text" name="emailAltern" size="30" maxlength="30"><br />
				</fieldset><br />
			<input type="submit" name="submit2" value="Search">
			</form>
			<form class="form2" action="results3.php" method="post">
				<fieldset>
					<label>Υπεύθυνοι Υπηρεσιών ΕΔΕΤ: </label><input type="text" name="edet" size="30" maxlength="40"><br />
				</fieldset><br />
			<input type="submit" name="submit2" value="Search">
			</form>
			
				
		<?php
	}
	
	public function DisplayFooter()
	{
		?>
			</section>
	</section>
		<footer>
			<div id="copyright-align-left">
				<p>© Copyright 2018 ucnet.uoc.gr</p>
			</div>
			<div id="copyright-align-right">
				<a href="http://www.uoc.gr/" target="_blank"><img alt="uoc logo small" src="uoc_logo_small.png" width="70" height="70"></a>
			</div>
		</footer>
		<?php
	}
}
?>