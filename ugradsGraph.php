<?php // content="text/plain; charset=utf-8"
	
	require("page.php");
	require_once ('jpgraph/src/jpgraph.php');
	require_once ('jpgraph/src/jpgraph_pie.php');
	require_once ('jpgraph/src/jpgraph_pie3d.php');

	/* access DB */	
	@$db = new mysqli('localhost', 'stats', 'stats123', 'statistics');
	
	if (mysqli_connect_errno()) {
       echo '<p>Error: Could not connect to database.<br/>
       Please try again later.</p>';
       exit;
    }
	
    $query = "SELECT Biology, Chemistry, Csd, Econ, Fks, Hist_Arch, Materials, Math, Tem,
			  Philology, Ptde, Ptpe, Social, Pol, Physics, Med, Psychology, Total 
			  FROM Ugrads ORDER BY Ts DESC LIMIT 1";
    $stmt = $db->prepare($query); 
    $stmt->execute();
    $stmt->store_result();
  
    $stmt->bind_result($biologyUgrads, $chemistryUgrads, $csdUgrads, $econUgrads, $fksUgrads, 
						$histArchUgrads, $materialsUgrads, $mathUgrads, $temUgrads, 
						$philologyUgrads, $ptdeUgrads, $ptpeUgrads, $socialUgrads, 
						$polUgrads, $physicsUgrads, $medUgrads, $psychologyUgrads, $total);

    while($stmt->fetch()) {
		$biologyUgrads; 
		$chemistryUgrads; 
		$csdUgrads; 
		$econUgrads; 
		$fksUgrads; 
		$histArchUgrads; 
		$materialsUgrads; 
		$mathUgrads; 
		$temUgrads; 
		$philologyUgrads; 
		$ptdeUgrads; 
		$ptpeUgrads; 
		$socialUgrads; 
		$polUgrads; 
		$physicsUgrads; 
		$medUgrads; 
		$psychologyUgrads;
		$total;
    }

    $stmt->free_result();
    $db->close();
		
	//percentages for ugrads pie	
	$biologyPerc = round(($biologyUgrads/$total)*100);
	$chemistryPerc = round(($chemistryUgrads/$total)*100); 
	$csdPerc = round(($csdUgrads/$total)*100); 
	$econPerc = round(($econUgrads/$total)*100); 
	$fksPerc = round(($fksUgrads/$total)*100); 
	$histArchPerc = round(($histArchUgrads/$total)*100); 
	$materialsPerc = round(($materialsUgrads/$total)*100); 
	$mathPerc = round(($mathUgrads/$total)*100); 
	$temPerc = round(($temUgrads/$total)*100); 
	$philologyPerc = round(($philologyUgrads/$total)*100); 
	$ptdePerc = round(($ptdeUgrads/$total)*100); 
	$ptpePerc = round(($ptpeUgrads/$total)*100); 
	$socialPerc = round(($socialUgrads/$total)*100); 
	$polPerc = round(($polUgrads/$total)*100); 
	$physicsPerc = round(($physicsUgrads/$total)*100); 
	$medPerc = round(($medUgrads/$total)*100); 
	$psychologyPerc = round(($psychologyUgrads/$total)*100); 
	
	// Some data
	$data = array($biologyPerc, $chemistryPerc, $csdPerc, $econPerc, $fksPerc, $histArchPerc,
				  $materialsPerc, $mathPerc, $temPerc, $philologyPerc, $ptdePerc, $ptpePerc, 
				  $socialPerc, $polPerc, $physicsPerc, $medPerc, $psychologyPerc);

	// Create the Pie Graph. 
	$graph = new PieGraph(660, 660);

	$theme_class= new UniversalTheme;
	$graph->SetTheme($theme_class);

	// Set A title for the plot
	$graph->title->Set("Προπτυχιακοί Φοιτητές Ανά Τμήμα");
	$graph->title->SetFont(FF_VERDANA,FS_BOLD,14);
	
	// Create
	$p1 = new PiePlot3D($data);
	$graph->Add($p1);

	$p1->ShowBorder();
	$p1->SetColor('black');
	$p1->SetSliceColors(array('#ff9446','#A02166','#2E8E2E','#560682', '#0b93b3', '#FFDB5A', 
							  '#8E8ED6', '#0A4800', '#0867B0', '#8006B0', '#34387B', '#73C567',
							  '#FF5306','#630014', '#045B86','#9A2D2D', '#524DCD'));
	$p1->SetStartAngle(52);
	$p1->value->SetFont(FF_FONT1,FS_BOLD,5);	
	$p1->value->SetColor('#FFFEF3');
	$p1->SetLabels($data,0.6);

	$p1->SetShadow();
	$p1->ExplodeAll();

	$p1->SetLegends(array("ΤΜΗΜΑ ΒΙΟΛΟΓΙΑΣ", "ΤΜΗΜΑ ΧΗΜΕΙΑΣ", "ΤΜΗΜΑ ΕΠΙΣΤ. ΥΠΟΛ.",
						  "ΤΜΗΜΑ ΟΙΚΟΝ. ΕΠΙΣΤΗΜΩΝ", "ΤΜΗΜΑ ΦΙΛ. & ΚΟΙN. ΣΠΟΥΔΩΝ",  
						  "ΤΜΗΜΑ ΙΣΤ. & ΑΡΧ.", "ΤΜΗΜΑ ΕΠΙΣΤ. & ΤΕΧΝ. ΥΛΙΚΩΝ", 
						  "ΤΜΗΜΑ ΜΑΘ.& ΕΦ. ΜΑΘ.-ΜΑΘ.", "ΤΜΗΜΑ ΜΑΘ.& ΕΦ. ΜΑΘ.-ΕΦ. ΜΑΘ.", 
						  "ΤΜΗΜΑ ΦΙΛΟΛΟΓΙΑΣ", "ΤΜΗΜΑ ΔΗΜ. ΕΚΠ. (ΠΑΙΔΑΓ.)", 
						  "ΤΜΗΜΑ ΠΡΟΣΧ. ΕΚΠ. (ΠΑΙΔΑΓ.)", "ΤΜΗΜΑ ΚΟΙΝΩΝΙΟΛ.", 
						  "ΤΜΗΜΑ ΠΟΛΙΤΙΚΗΣ ΕΠΙΣΤΗΜΗΣ", "ΤΜΗΜΑ ΦΥΣΙΚΗΣ", "ΤΜΗΜΑ ΙΑΤΡΙΚΗΣ", 
						  "ΤΜΗΜΑ ΨΥΧΟΛΟΓΙΑΣ"));
	$graph->legend->SetAbsPos(0,600,'right','center');
	$graph->legend->SetColumns(3);

	$gdImgHandler = $graph->Stroke(_IMG_HANDLER);

	$fileName = "tmp/ugrads_pie.png";
	$graph->img->Stream($fileName);
 
	// Send it back to browser
	$graph->img->Headers();
	$graph->img->Stream();
?>